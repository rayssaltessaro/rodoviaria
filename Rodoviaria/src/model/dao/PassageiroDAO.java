package model.dao;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Passageiro;

public class PassageiroDAO {


	public void create(Passageiro P) {
		
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		  
		try {
			stmt = con.prepareStatement("INSERT INTO passageiro (nome, genero, endereco, email,telefone, rg, cpf) VALUES (?, ?, ?, ?, ?, ?, ?)");
			stmt.setString(1, P.getNome());
			stmt.setBoolean(2, P.isGenero());
			stmt.setString(3, P.getEndereco());
			stmt.setString(4, P.getEmail());
			stmt.setString(5, P.getTelefone());
			stmt.setString(6, P.getRg());
			stmt.setString(7, P.getCpf());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	  public List <Passageiro> read(){
		  Connection con = ConnectionFactory.getConnection();
		  PreparedStatement stmt = null;
		  ResultSet rs = null;
		  List<Passageiro> passageiros = new ArrayList <>();
		  
		  try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while (rs.next()) {
				Passageiro p= new Passageiro();
				p.setID_passageiro(rs.getInt("ID_passageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getBoolean("genero"));
				p.setEndereco(rs.getString("endereco"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getString("telefone"));
				p.setRg(rs.getString("rg"));
				p.setCpf(rs.getString("cpf"));
				
				passageiros.add(p);	
								
				
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informações do BD" + e);
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		  return passageiros;
	  }
	  
	  public void delete (Passageiro p) {
		  
			Connection con = ConnectionFactory.getConnection();
			PreparedStatement stmt = null;	 
			
			try {
				stmt = con.prepareStatement( "DELETE FROM passageiro WHERE ID_passageiro=?");
				stmt.setInt(1, p.getID_passageiro());
				stmt.executeUpdate();
				
				JOptionPane.showMessageDialog(null, "Passageiro excluido com sucesso!");
			} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: " + e);
			} finally {
				ConnectionFactory.closeConnection(con, stmt);
			} }

	  
			
			public Passageiro read(int id) {
				Connection con = ConnectionFactory.getConnection();
				PreparedStatement stmt = null;
				ResultSet rs = null;
				Passageiro p = new Passageiro();
				
				try {
					stmt = con.prepareStatement("SELECT * FROM passageiro WHERE ID_passageiro=? LIMIT 1;");
					stmt.setInt(1, id);
					rs = stmt.executeQuery();
					if(rs != null && rs.next()) {
						p.setID_passageiro(rs.getInt("ID_passageiro"));
						p.setNome(rs.getString("nome"));
						p.setGenero(rs.getBoolean("genero"));
						p.setEndereco(rs.getString("endereco"));
						p.setEmail(rs.getString("email"));
						p.setTelefone(rs.getString("telefone"));
						p.setRg(rs.getString("rg"));
						p.setCpf(rs.getString("cpf"));					
						
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}finally {
					ConnectionFactory.closeConnection(con, stmt, rs);
				}
				return p;
			}
			
			public void update( Passageiro p) {
				Connection con = ConnectionFactory.getConnection();
				PreparedStatement stmt = null;
				
				try {
					stmt = con.prepareStatement("UPDATE passageiro SET nome=?, genero=?, endereco=?, email=?, telefone=?, rg=?, cpf=? WHERE ID_Passageiro=?;");
					stmt.setString(1, p.getNome());
					stmt.setBoolean(2, p.isGenero());
					stmt.setString(3, p.getEndereco());
					stmt.setString(4, p.getEmail());
					stmt.setString(5, p.getTelefone());
					stmt.setString(6, p.getRg());
					stmt.setString(7, p.getCpf());
					stmt.executeUpdate();
					
					JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
				}catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
				} finally {
					ConnectionFactory.closeConnection(con, stmt);
				}
			}
				
		}
