package model.bean;
 
public class Passageiro {
	
	private int ID_passageiro;  
	private String nome;
	private boolean genero;
	private String endereco;
	private String email;
	private String telefone;
	private String rg;
	private String cpf;
	public int getID_passageiro() {
		return ID_passageiro;
	}
	public void setID_passageiro(int iD_passageiro) {
		ID_passageiro = iD_passageiro;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public boolean isGenero() {
		return genero;
	}
	public void setGenero(boolean genero) {
		this.genero = genero;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	


}
