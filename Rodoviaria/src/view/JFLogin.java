package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFLogin extends JFrame {

	private JPanel contentPane;
	private JTextField botUsuario;
	private JPasswordField botSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setTitle("Sis. Rodoviaria - Tela de Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Sistema Rodoviaria - Seja Bem Vindo!");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setBounds(119, 11, 248, 27);
		contentPane.add(lblNewLabel);
		
		JLabel txtInfo = new JLabel("Informe abaixo as suas credenciais de acesso!");
		txtInfo.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		txtInfo.setBounds(109, 64, 274, 14);
		contentPane.add(txtInfo);
		
		JLabel txtUsuario = new JLabel("Usu\u00E1rio:");
		txtUsuario.setFont(new Font("Times New Roman", Font.BOLD, 12));
		txtUsuario.setBounds(60, 92, 46, 14);
		contentPane.add(txtUsuario);
		
		botUsuario = new JTextField();
		botUsuario.setBounds(116, 89, 239, 20);
		contentPane.add(botUsuario);
		botUsuario.setColumns(10);
		
		JLabel txtSenha = new JLabel("Senha:");
		txtSenha.setFont(new Font("Times New Roman", Font.BOLD, 13));
		txtSenha.setBounds(60, 120, 46, 14);
		contentPane.add(txtSenha);
		
		botSenha = new JPasswordField();
		botSenha.setBounds(116, 120, 239, 20);
		contentPane.add(botSenha);
		
		JButton botAcesso = new JButton("Acessar");
		botAcesso.setForeground(Color.BLACK);
		botAcesso.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 13));
		botAcesso.setBackground(Color.WHITE);
		botAcesso.setBounds(60, 180, 155, 23);
		contentPane.add(botAcesso);
		
		JButton botLimpa = new JButton("Limpar campos");
		botLimpa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		botLimpa.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 13));
		botLimpa.setBackground(Color.WHITE);
		botLimpa.setBounds(228, 180, 155, 23);
		contentPane.add(botLimpa);
		
		JButton botCadastra = new JButton("Cadastrar-se");
		botCadastra.setBackground(Color.WHITE);
		botCadastra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		botCadastra.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 13));
		botCadastra.setBounds(60, 227, 155, 23);
		contentPane.add(botCadastra);
		
		JButton botRecupera = new JButton("Recuperar senha");
		botRecupera.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 13));
		botRecupera.setBackground(Color.WHITE);
		botRecupera.setBounds(228, 227, 155, 23);
		contentPane.add(botRecupera);
	}
}
