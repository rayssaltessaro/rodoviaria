package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;

public class JFAlterarPassageiro extends JFrame {

	private static int id;
	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone;
	private JTextField txtRG;
	private JTextField txtCPF;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAlterarPassageiro frame = new JFAlterarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFAlterarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 528, 360);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO pdao = new PassageiroDAO();
		Passageiro p = pdao.read(id);
		
		
		JLabel Id = new JLabel("ID:");
		Id.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		Id.setBounds(10, 74, 46, 14);
		contentPane.add(Id);
		
		JLabel lblID = new JLabel("New label");
		lblID.setBounds(55, 75, 46, 14);
		contentPane.add(lblID);
		
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblNome.setBounds(10, 99, 46, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(55, 97, 190, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblGenero = new JLabel("Genero:");
		lblGenero.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblGenero.setBounds(10, 211, 77, 14);
		contentPane.add(lblGenero);
				
		JRadioButton rdbtnFeminino = new JRadioButton("Feminino");
		rdbtnFeminino.setBackground(Color.WHITE);
		rdbtnFeminino.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		rdbtnFeminino.setBounds(64, 207, 80, 23);
		contentPane.add(rdbtnFeminino);
		
		JRadioButton rdbtnMasculino = new JRadioButton("Masculino");
		rdbtnMasculino.setBackground(Color.WHITE);
		rdbtnMasculino.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		rdbtnMasculino.setBounds(146, 207, 109, 23);
		contentPane.add(rdbtnMasculino);
		
		ButtonGroup genero = new ButtonGroup();
		genero.add(rdbtnFeminino);
		genero.add(rdbtnMasculino);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o:");
		lblEndereco.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblEndereco.setBounds(248, 99, 70, 14);
		contentPane.add(lblEndereco);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(312, 97, 190, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblEmail.setBounds(10, 130, 46, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(55, 128, 190, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblTelefone.setBounds(248, 130, 65, 14);
		contentPane.add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(312, 128, 190, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblRG = new JLabel("RG:");
		lblRG.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblRG.setBounds(10, 161, 46, 14);
		contentPane.add(lblRG);
		
		txtRG = new JTextField();
		txtRG.setBounds(55, 159, 190, 20);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		JLabel lblCPF = new JLabel("CPF:");
		lblCPF.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblCPF.setBounds(256, 161, 46, 14);
		contentPane.add(lblCPF);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(312, 159, 190, 20);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		JLabel lblTitulo = new JLabel("Cadastro Passageiro");
		lblTitulo.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblTitulo.setBounds(169, 26, 149, 33);
		contentPane.add(lblTitulo);
		txtNome.setText(p.getNome());
		if(p.isGenero() == true) {
			rdbtnFeminino.setSelected(true);;
		}else if(p.isGenero() == false) {
			rdbtnMasculino.setSelected(true);
			}
		txtEndereco.setText(p.getEndereco());
		txtEmail.setText(p.getEmail());
		txtTelefone.setText(p.getTelefone());
		txtRG.setText(p.getRg());
		txtCPF.setText(p.getCpf());
		
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro P = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				P.setID_passageiro(Integer.parseInt(lblID.getText()));
				
				P.setNome(txtNome.getText());
				if(rdbtnFeminino.isSelected()) {
					P.setGenero(false);
			}else if (rdbtnMasculino.isSelected()) {
				P.setGenero(true);
		}
				P.setEndereco(txtEndereco.getText());
				P.setEmail(txtEmail.getText());
				P.setTelefone(txtTelefone.getText());
				P.setRg(txtRG.getText());
				P.setCpf(txtCPF.getText());
				
				dao.update(P);
			}
			
		});
		btnAlterar.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		btnAlterar.setBounds(44, 256, 100, 34);
		contentPane.add(btnAlterar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		btnLimpar.setBounds(208, 256, 100, 34);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		btnCancelar.setBounds(359, 256, 100, 34);
		contentPane.add(btnCancelar);

	}
}
