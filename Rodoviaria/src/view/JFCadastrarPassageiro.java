package view;
 
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastrarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone; 
	private JTextField txtRG;
	private JTextField txtCPF;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try { 
					JFCadastrarPassageiro frame = new JFCadastrarPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastrarPassageiro() {
		setTitle("Sis. Rodovi\u00E1ria - Cadastrar Passageiro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 528, 360);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblNome.setBounds(10, 57, 46, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(54, 55, 190, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblGenero = new JLabel("Genero:");
		lblGenero.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblGenero.setBounds(10, 187, 77, 14);
		contentPane.add(lblGenero);
				
		JRadioButton rdbtnFeminino = new JRadioButton("Feminino");
		rdbtnFeminino.setBackground(Color.WHITE);
		rdbtnFeminino.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		rdbtnFeminino.setBounds(64, 183, 80, 23);
		contentPane.add(rdbtnFeminino);
		
		JRadioButton rdbtnMasculino = new JRadioButton("Masculino");
		rdbtnMasculino.setBackground(Color.WHITE);
		rdbtnMasculino.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		rdbtnMasculino.setBounds(146, 183, 109, 23);
		contentPane.add(rdbtnMasculino);
		
		ButtonGroup genero = new ButtonGroup();
		genero.add(rdbtnFeminino);
		genero.add(rdbtnMasculino);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o:");
		lblEndereco.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblEndereco.setBounds(248, 57, 70, 14);
		contentPane.add(lblEndereco);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(312, 55, 190, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblEmail.setBounds(10, 99, 46, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(54, 97, 190, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblTelefone.setBounds(248, 99, 65, 14);
		contentPane.add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(312, 97, 190, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblRG = new JLabel("RG:");
		lblRG.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblRG.setBounds(10, 142, 46, 14);
		contentPane.add(lblRG);
		
		txtRG = new JTextField();
		txtRG.setBounds(55, 140, 190, 20);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		JLabel lblCPF = new JLabel("CPF:");
		lblCPF.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		lblCPF.setBounds(248, 142, 46, 14);
		contentPane.add(lblCPF);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(312, 140, 190, 20);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		JLabel lblTitulo = new JLabel("Cadastro Passageiro");
		lblTitulo.setFont(new Font("Script MT Bold", Font.PLAIN, 17));
		lblTitulo.setBounds(191, 11, 149, 33);
		contentPane.add(lblTitulo);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro P = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				P.setNome(txtNome.getText());
				if(rdbtnFeminino.isSelected()) {
					P.setGenero(false);
			}else if (rdbtnMasculino.isSelected()) {
				P.setGenero(true);
							}
				P.setEndereco(txtEndereco.getText());
				P.setEmail(txtEmail.getText());
				P.setTelefone(txtTelefone.getText());
				P.setRg(txtRG.getText());
				P.setCpf(txtCPF.getText());
				
				dao.create(P);
			}
			
		});
		btnCadastrar.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		btnCadastrar.setBounds(44, 256, 100, 34);
		contentPane.add(btnCadastrar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		btnLimpar.setBounds(208, 256, 100, 34);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Lucida Bright", Font.ITALIC, 12));
		btnCancelar.setBounds(359, 256, 100, 34);
		contentPane.add(btnCancelar);
	}
}
