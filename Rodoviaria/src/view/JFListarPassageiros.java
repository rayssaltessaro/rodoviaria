package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JT_Passageiro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 675, 406);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel listar = new JLabel("Listar Passageiros");
		listar.setFont(new Font("Lucida Bright", Font.BOLD | Font.ITALIC, 12));
		listar.setBounds(227, 11, 120, 21);
		contentPane.add(listar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 43, 639, 189);
		contentPane.add(scrollPane);
		
		JT_Passageiro = new JTable();
		JT_Passageiro.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
			},
			new String[] {
				"ID_Passageiro", "Nome", "G\u00EAnero", "Endere\u00E7o", "Email", "Telefone", "RG", "CPF"
			}
		));
		scrollPane.setViewportView(JT_Passageiro);
		
		JButton btnCadastrar = new JButton("Cadastrar Passageiro");
		btnCadastrar.setBackground(Color.WHITE);
		btnCadastrar.setFont(new Font("Sitka Small", Font.ITALIC, 11));
		btnCadastrar.setBounds(30, 266, 159, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnAlterar = new JButton("Alterar Passageiro");
		btnAlterar.setBackground(Color.WHITE);
		btnAlterar.setFont(new Font("Sitka Small", Font.ITALIC, 11));
		btnAlterar.setBounds(231, 266, 159, 23);
		contentPane.add(btnAlterar);
		
		
		
		JButton btnExcluir = new JButton("Excluir Passageiro");
		btnExcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(JT_Passageiro.getSelectedRow()!= -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o passageiro selecionado?","Exclus�o", JOptionPane.YES_NO_OPTION);
					if(opcao == 0) {
						PassageiroDAO dao = new PassageiroDAO();
						Passageiro p = new Passageiro();
						p.setID_passageiro((int)JT_Passageiro.getValueAt(JT_Passageiro.getSelectedRow(),0));
						dao.delete(p);
					}
				}else {
					JOptionPane.showMessageDialog(null,"Selecione um passageiro!");
				}
				readJTable();
			}
		});
		
		btnExcluir.setBackground(Color.WHITE);
		btnExcluir.setForeground(new Color(0, 0, 0));
		btnExcluir.setFont(new Font("Sitka Small", Font.ITALIC, 11));
		btnExcluir.setBounds(429, 266, 159, 23);
		contentPane.add(btnExcluir);
		
		readJTable();
	} 
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JT_Passageiro.getModel();
		modelo.setNumRows(0);
		PassageiroDAO pdao = new PassageiroDAO();
		for (Passageiro P : pdao.read()) {
			modelo.addRow(new Object[] {
					P.getID_passageiro(),
					P.getNome(),
					P.isGenero(),
					P.getEndereco(),
					P.getEmail(),
					P.getTelefone(),
					P.getRg(),
					P.getCpf()
			});
		}
	}
		
	}
